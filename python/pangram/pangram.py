import string

letters = set(string.ascii_lowercase)


def is_pangram(s):
    """Return True if s uses all letters in alphabet"""
    return len(letters - set(s.lower())) == 0
