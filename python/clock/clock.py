"""Clock implementation"""

DAY = 60 * 24


class Clock:
    """A clock."""
    def __init__(self, hour=0, minute=0):
        self.minutes = 0
        if hour != 0 or minute != 0:
            self.add(60*hour + minute)

    def add(self, minutes):
        if minutes < 0:
            minutes = DAY + (minutes % DAY)

        self.minutes = (self.minutes + minutes) % DAY
        return self

    def __eq__(self, other):
        return self.minutes == other.minutes

    def __str__(self):
        hour, minute = divmod(self.minutes, 60)
        return '%02d:%02d' % (hour % 24, minute)
