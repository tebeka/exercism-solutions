package bob // package name must match the package name in bob_test.go

import (
	"regexp"
	"strings"
)

const testVersion = 2 // same as targetTestVersion

var (
	hasChars = regexp.MustCompile("[a-zA-Z]").MatchString
)

// Hey returns Bob's answer to greeting
func Hey(greeting string) string {
	greeting = strings.TrimSpace(greeting)
	switch {
	case hasChars(greeting) && strings.ToUpper(greeting) == greeting:
		return "Whoa, chill out!"
	case len(greeting) == 0:
		return "Fine. Be that way!"
	case greeting[len(greeting)-1] == '?':
		return "Sure."
	}

	return "Whatever."
}
