package secret

var (
	flags = []struct {
		Value  int
		Action string
	}{
		{1 << 0, "wink"},
		{1 << 1, "double blink"},
		{1 << 2, "close your eyes"},
		{1 << 3, "jump"},
	}
)

const (
	reverse = 1 << 4
)

// Handshake return handshake actions according to flags in n
func Handshake(n int) []string {
	var hs []string

	if n <= 0 {
		return hs
	}

	for _, flg := range flags {
		if n&flg.Value != 0 {
			hs = append(hs, flg.Action)
		}
	}

	if n&reverse != 0 {
		for i, j := 0, len(hs)-1; i < j; i, j = i+1, j-1 {
			hs[i], hs[j] = hs[j], hs[i]
		}
	}

	return hs
}
