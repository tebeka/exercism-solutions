package triangle

import "math"

const testVersion = 2

// Kind is triangle king
type Kind int

const (
	NaT Kind = iota // not a triangle
	Equ             // equilateral
	Iso             // isosceles
	Sca             // scalene
)

// isValid return true if n is a valid number
func isValid(n float64) bool {
	switch {
	case n == 0.0:
		return false
	case math.IsInf(n, 1):
		return false
	case math.IsInf(n, -1):
		return false
	case math.IsNaN(n):
		return false
	}

	return true
}

// KindFromSides return kind of triangle
func KindFromSides(a, b, c float64) Kind {
	if !isValid(a) || !isValid(b) || !isValid(c) {
		return NaT
	}

	if (a+b < c) || (a+c < b) || (b+c < a) {
		return NaT
	}

	switch {
	case a == b && b == c:
		return Equ
	case a == b || b == c || a == c:
		return Iso
	}
	return Sca
}
