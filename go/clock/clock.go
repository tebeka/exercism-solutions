package clock

import "fmt"

const (
	testVersion = 4
	day         = 24 * 60
)

type Clock int

// New returns new Clock
func New(hour, minute int) Clock {
	return Clock(0).Add(60*hour + minute)
}

// Minute returns minute part of click
func (c Clock) Minute() int {
	return int(c) % 60
}

// Hour returns hour part of click
func (c Clock) Hour() int {
	return int(c) / 60
}

// String return string represtation of time in HH:MM format
func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.Hour()%24, c.Minute())
}

// Adds add minutes to the current time.
// minutes can be negative
func (c Clock) Add(minutes int) Clock {
	if minutes < 0 {
		minutes = day + minutes%day
	}
	return Clock((int(c) + minutes) % day)
}
