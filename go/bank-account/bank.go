package account

import "sync"

// Account is a bank account
type Account struct {
	m       sync.Mutex
	balance int
	active  bool
}

// Open opens the account with amount as balance
func Open(amount int) *Account {
	if amount < 0 {
		return nil
	}
	return &Account{
		balance: amount,
		active:  true,
	}
}

// Balance return the current balance, ok will be false if account is closed
func (a *Account) Balance() (int, bool) {
	a.m.Lock()
	defer a.m.Unlock()

	if !a.active {
		return 0, false
	}
	return a.balance, true
}

// Deposit will deposit amount in account. If amount is negative it's a
// withdrawl
func (a *Account) Deposit(amount int) (int, bool) {
	a.m.Lock()
	defer a.m.Unlock()

	if !a.active {
		return 0, false
	}

	balance := a.balance + amount
	if balance < 0 {
		return 0, false
	}
	a.balance = balance

	return balance, true
}

// Close closes the account
func (a *Account) Close() (int, bool) {
	a.m.Lock()
	defer a.m.Unlock()

	if !a.active {
		return 0, false
	}

	a.active = false
	return a.balance, true
}
