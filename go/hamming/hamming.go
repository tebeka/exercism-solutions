package hamming

import "fmt"

const testVersion = 4

// Distance return hamming distance between a and b
// It'll return -1 and an error if the length of a and b differ
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return -1, fmt.Errorf("len(a) != len(b) (%d != %d)", len(a), len(b))
	}

	dist := 0
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			dist++
		}
	}

	return dist, nil
}
