package gigasecond

import "time"

const (
	testVersion = 4
	billion     = 1000000000 * time.Second
)

// API function.  It uses a type from the Go standard library.
// AddGigasecond adds 10^9 seconds to time
func AddGigasecond(t time.Time) time.Time {
	return t.Add(billion)
}
