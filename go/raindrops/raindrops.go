package raindrops

import "fmt"

const testVersion = 2

var (
	factors = []struct {
		Factor int
		Name   string
	}{
		{3, "Pling"},
		{5, "Plang"},
		{7, "Plong"},
	}
)

// Convert convert number to PlingPlangPlong string
func Convert(n int) string {
	result := ""
	for _, fact := range factors {
		if n%fact.Factor == 0 {
			result += fact.Name
		}
	}

	if len(result) == 0 {
		return fmt.Sprintf("%d", n)
	}

	return result
}
