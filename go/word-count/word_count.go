package wordcount

import (
	"regexp"
	"strings"
)

const testVersion = 2

// Use this return type.
type Frequency map[string]int

var (
	findWords = regexp.MustCompile("[a-z0-9]+").FindAllString
)

// WordCount return frequency of words
func WordCount(phrase string) Frequency {
	freq := Frequency{}
	for _, word := range findWords(strings.ToLower(phrase), -1) {
		freq[word] += 1
	}

	return freq
}
